import React from 'react'
import { Provider } from 'react-redux'
import { PersistGate } from 'redux-persist/integration/react'
import { Root } from 'native-base'
import Redux from './Redux'
import Navigator from './Navigator'

const { store, persistor } = Redux()

export default () => (
    <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
            <Root>
                <Navigator />
            </Root>
        </PersistGate>
    </Provider>
)
