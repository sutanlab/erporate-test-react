import { combineReducers } from 'redux'
import product from './Product'
import article from './Article'

export default combineReducers({ product, article })
