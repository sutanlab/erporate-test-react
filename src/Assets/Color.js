export const Primary = '#6aca8b'
export const Accent = '#0099de'

export default {
    Primary,
    Accent
}
