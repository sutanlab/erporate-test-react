import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { StyleSheet, Image } from 'react-native'
import {
    View,
    Text,
    Content,
    Card,
    CardItem,
    Body,
    Button,
    Right,
    Icon
} from 'native-base'
import { getArticles } from '../Redux/Actions/Article'
import Header from '../Components/Header'
import Loader from '../Components/Loader'
import Color from '../Assets/Color'

const CardArticle = ({ data, navigate }) =>
    data.map((article, idx) => (
        <Card key={idx}>
            <CardItem>
                <Body>
                    <Image
                        source={{ uri: article.urlToImage }}
                        style={styles.image}
                    />
                    <View style={styles.horizontalLine} />
                    <Text style={styles.title}>{article.title}</Text>
                    <Text style={{ marginTop: 10 }}>{article.description}</Text>
                </Body>
            </CardItem>
            <CardItem>
                <Right style={styles.action}>
                    <Button
                        block
                        onPress={() => navigate('DetailArticle', { article })}
                        style={{
                            backgroundColor: Color.Primary
                        }}
                    >
                        <Text>Detail</Text>
                    </Button>
                </Right>
            </CardItem>
        </Card>
    ))

export default ({ navigation }) => {
    const dispatch = useDispatch()
    const { data: articles, loading } = useSelector(({ article }) => article)
    const fetchArticles = () => dispatch(getArticles())

    return (
        <>
            <Header
                title="Articles"
                rightComponent={
                    <Button
                        transparent
                        disabled={loading}
                        onPress={fetchArticles}
                    >
                        <Icon name="refresh" />
                    </Button>
                }
            />
            <Content padder>
                {loading ? (
                    <Loader />
                ) : (
                    <CardArticle
                        data={articles}
                        navigate={navigation.navigate}
                    />
                )}
            </Content>
        </>
    )
}

const styles = StyleSheet.create({
    image: {
        height: 200,
        width: '100%',
        resizeMode: 'contain'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20
    },
    horizontalLine: {
        width: '85%',
        backgroundColor: '#ddd',
        alignSelf: 'center',
        height: 3,
        marginVertical: 10
    },
    action: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    }
})
