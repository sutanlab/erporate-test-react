import React from 'react'
import { StyleSheet, Image } from 'react-native'
import { View, Text, Button, Icon, Content } from 'native-base'
import Header from '../Components/Header'
import Color from '../Assets/Color'

export default ({ navigation }) => {
    const data = navigation.getParam('product')
    return (
        <>
            <Header
                title={data.name}
                leftComponent={
                    <Button transparent onPress={() => navigation.goBack()}>
                        <Icon name="arrow-back" />
                    </Button>
                }
            />
            <Content padder>
                <Image
                    style={styles.image}
                    source={{
                        uri: `https://sutanlab-backend-pos.herokuapp.com/files/image/product/${
                            data.image
                        }`
                    }}
                />
                <Text style={styles.title}>{data.name}</Text>
                <Text note style={styles.subtitle}>
                    {data.Category.name}
                </Text>
                <View style={styles.line} />
                <Text style={styles.content}>{data.description}</Text>
            </Content>
        </>
    )
}

const styles = StyleSheet.create({
    title: {
        fontSize: 22,
        textAlign: 'center',
        fontWeight: 'bold',
        marginTop: 10
    },
    subtitle: {
        fontSize: 18,
        textAlign: 'center'
    },
    content: {
        textAlign: 'center'
    },
    line: {
        width: '85%',
        height: 5,
        marginVertical: 15,
        backgroundColor: '#ddd',
        alignSelf: 'center'
    },
    image: {
        width: '100%',
        height: 200,
        resizeMode: 'contain'
    },
    readMore: {
        backgroundColor: Color.Primary,
        marginTop: 20
    }
})
