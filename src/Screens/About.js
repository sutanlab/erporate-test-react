import React from 'react'
import { StyleSheet } from 'react-native'
import { View, Text, Content } from 'native-base'
import { WebView } from 'react-native-webview'
import Header from '../Components/Header'

export default ({ navigation }) => {
    return (
        <>
            <Header title="About Us" />
            <Content padder>
                <View
                    style={{
                        height: 200,
                        width: '100%',
                        marginVertical: 10
                    }}
                >
                    <WebView
                        source={{
                            html:
                                '<html><body><iframe style="width: 100%; height: 100%" src="https://www.youtube.com/embed/wmrfFsPnvVA"  allowfullscreen></iframe></body></html>'
                        }}
                    />
                </View>
                <View style={styles.container}>
                    <Text style={styles.title}>About Us</Text>
                    <View style={styles.hr} />
                    <Text style={{ textAlign: 'center', marginVertical: 10 }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum.
                    </Text>
                    <Text style={{ textAlign: 'center' }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua. Ut enim ad minim veniam, quis nostrud
                        exercitation ullamco laboris nisi ut aliquip ex ea
                        commodo consequat. Duis aute irure dolor in
                        reprehenderit in voluptate velit esse cillum dolore eu
                        fugiat nulla pariatur. Excepteur sint occaecat cupidatat
                        non proident, sunt in culpa qui officia deserunt mollit
                        anim id est laborum.
                    </Text>
                </View>
            </Content>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    title: {
        fontSize: 22,
        fontWeight: 'bold'
    },
    hr: {
        height: 3,
        width: '40%',
        backgroundColor: '#ddd',
        marginVertical: 5
    }
})
